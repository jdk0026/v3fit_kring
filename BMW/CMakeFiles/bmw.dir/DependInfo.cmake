# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/bmw_commandline_parser.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/bmw_commandline_parser.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/bmw_context.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/bmw_context.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/bmw_parallel_context.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/bmw_parallel_context.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/bmw_state_flags.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/bmw_state_flags.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/m_grid.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/m_grid.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/primed_grid.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/primed_grid.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/siesta_file.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/siesta_file.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/unprimed_grid.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/Sources/unprimed_grid.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules/bmw")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

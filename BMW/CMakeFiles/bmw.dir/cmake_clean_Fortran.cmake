# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/bmw/bmw_commandline_parser.mod"
  "../build/modules/bmw/BMW_COMMANDLINE_PARSER.mod"
  "CMakeFiles/bmw.dir/bmw_commandline_parser.mod.stamp"

  "../build/modules/bmw/bmw_context.mod"
  "../build/modules/bmw/BMW_CONTEXT.mod"
  "CMakeFiles/bmw.dir/bmw_context.mod.stamp"

  "../build/modules/bmw/bmw_parallel_context.mod"
  "../build/modules/bmw/BMW_PARALLEL_CONTEXT.mod"
  "CMakeFiles/bmw.dir/bmw_parallel_context.mod.stamp"

  "../build/modules/bmw/bmw_state_flags.mod"
  "../build/modules/bmw/BMW_STATE_FLAGS.mod"
  "CMakeFiles/bmw.dir/bmw_state_flags.mod.stamp"

  "../build/modules/bmw/m_grid.mod"
  "../build/modules/bmw/M_GRID.mod"
  "CMakeFiles/bmw.dir/m_grid.mod.stamp"

  "../build/modules/bmw/primed_grid.mod"
  "../build/modules/bmw/PRIMED_GRID.mod"
  "CMakeFiles/bmw.dir/primed_grid.mod.stamp"

  "../build/modules/bmw/siesta_file.mod"
  "../build/modules/bmw/SIESTA_FILE.mod"
  "CMakeFiles/bmw.dir/siesta_file.mod.stamp"

  "../build/modules/bmw/unprimed_grid.mod"
  "../build/modules/bmw/UNPRIMED_GRID.mod"
  "CMakeFiles/bmw.dir/unprimed_grid.mod.stamp"
  )

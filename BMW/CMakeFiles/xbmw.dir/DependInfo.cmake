# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/BMW/Sources/bmw.f" "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/xbmw.dir/Sources/bmw.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/inst/jdk0026/v3fit/trunk/BMW/CMakeFiles/bmw.dir/DependInfo.cmake"
  "/home/inst/jdk0026/v3fit/trunk/LIBSTELL/CMakeFiles/stell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  "build/modules/bmw"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/xbootsj/parambs.mod"
  "../build/modules/xbootsj/PARAMBS.mod"
  "CMakeFiles/xbootsj.dir/parambs.mod.stamp"

  "../build/modules/xbootsj/trig.mod"
  "../build/modules/xbootsj/TRIG.mod"
  "CMakeFiles/xbootsj.dir/trig.mod.stamp"

  "../build/modules/xbootsj/vmec0.mod"
  "../build/modules/xbootsj/VMEC0.mod"
  "CMakeFiles/xbootsj.dir/vmec0.mod.stamp"
  )

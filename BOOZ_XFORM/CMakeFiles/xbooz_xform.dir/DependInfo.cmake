# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/allocate_boozer.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/allocate_boozer.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/booz_jac.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/booz_jac.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/booz_params.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/booz_params.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/booz_persistent.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/booz_persistent.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/boozer.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/boozer.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/boozer_coords.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/boozer_coords.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/boozer_xform.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/boozer_xform.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/foranl.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/foranl.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/free_mem_boozer.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/free_mem_boozer.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/harfun.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/harfun.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/read_wout_booz.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/read_wout_booz.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/setup_booz.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/setup_booz.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/transpmn.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/transpmn.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/trigfunc.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/trigfunc.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/vcoords.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/vcoords.f.o"
  "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/Sources/write_boozmn.f" "/home/inst/jdk0026/v3fit/trunk/BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/Sources/write_boozmn.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/inst/jdk0026/v3fit/trunk/LIBSTELL/CMakeFiles/stell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules/xbooz_xform")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

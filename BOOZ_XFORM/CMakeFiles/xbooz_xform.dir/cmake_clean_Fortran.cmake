# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/xbooz_xform/booz_params.mod"
  "../build/modules/xbooz_xform/BOOZ_PARAMS.mod"
  "CMakeFiles/xbooz_xform.dir/booz_params.mod.stamp"

  "../build/modules/xbooz_xform/booz_persistent.mod"
  "../build/modules/xbooz_xform/BOOZ_PERSISTENT.mod"
  "CMakeFiles/xbooz_xform.dir/booz_persistent.mod.stamp"
  )

# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk
# Build directory: /home/inst/jdk0026/v3fit/trunk
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(LIBSTELL)
subdirs(VMEC2000)
subdirs(POINCARE)
subdirs(SIESTA)
subdirs(V3FITA)
subdirs(MAKEGRID)
subdirs(V3RFUN)
subdirs(BOOZ_XFORM)
subdirs(WOUT_CONVERTER)
subdirs(BOOTSJ)
subdirs(DESCUR)
subdirs(BMW)
subdirs(LGRID)
subdirs(Testing)

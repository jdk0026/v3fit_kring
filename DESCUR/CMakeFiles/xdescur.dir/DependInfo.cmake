# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/amplitud.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/amplitud.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/descur.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/descur.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/evolve.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/evolve.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/fftrans.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/fftrans.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/fixaray.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/fixaray.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/funct.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/funct.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/getangle.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/getangle.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/getrz.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/getrz.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/inguess.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/inguess.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/order.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/order.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/printit.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/printit.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/read_indata.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/read_indata.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/read_wout_curve.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/read_wout_curve.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/restart.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/restart.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/scrunch.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/scrunch.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/vname0.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/vname0.f.o"
  "/home/inst/jdk0026/v3fit/trunk/DESCUR/Sources/vname1.f" "/home/inst/jdk0026/v3fit/trunk/DESCUR/CMakeFiles/xdescur.dir/Sources/vname1.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/inst/jdk0026/v3fit/trunk/LIBSTELL/CMakeFiles/stell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules/xdescur")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

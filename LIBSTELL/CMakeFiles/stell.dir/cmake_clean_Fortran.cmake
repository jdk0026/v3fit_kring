# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/stell/ajax_mod.mod"
  "../build/modules/stell/AJAX_MOD.mod"
  "CMakeFiles/stell.dir/ajax_mod.mod.stamp"

  "../build/modules/stell/bcoils_mod.mod"
  "../build/modules/stell/BCOILS_MOD.mod"
  "CMakeFiles/stell.dir/bcoils_mod.mod.stamp"

  "../build/modules/stell/biotsavart.mod"
  "../build/modules/stell/BIOTSAVART.mod"
  "CMakeFiles/stell.dir/biotsavart.mod.stamp"

  "../build/modules/stell/bnorm_mod.mod"
  "../build/modules/stell/BNORM_MOD.mod"
  "CMakeFiles/stell.dir/bnorm_mod.mod.stamp"

  "../build/modules/stell/bootsj_input.mod"
  "../build/modules/stell/BOOTSJ_INPUT.mod"
  "CMakeFiles/stell.dir/bootsj_input.mod.stamp"

  "../build/modules/stell/boundary_t.mod"
  "../build/modules/stell/BOUNDARY_T.mod"
  "CMakeFiles/stell.dir/boundary_t.mod.stamp"

  "../build/modules/stell/bsc_cdf.mod"
  "../build/modules/stell/BSC_CDF.mod"
  "CMakeFiles/stell.dir/bsc_cdf.mod.stamp"

  "../build/modules/stell/bsc_t.mod"
  "../build/modules/stell/BSC_T.mod"
  "CMakeFiles/stell.dir/bsc_t.mod.stamp"

  "../build/modules/stell/coilsnamin.mod"
  "../build/modules/stell/COILSNAMIN.mod"
  "CMakeFiles/stell.dir/coilsnamin.mod.stamp"

  "../build/modules/stell/compression.mod"
  "../build/modules/stell/COMPRESSION.mod"
  "CMakeFiles/stell.dir/compression.mod.stamp"

  "../build/modules/stell/control_mod.mod"
  "../build/modules/stell/CONTROL_MOD.mod"
  "CMakeFiles/stell.dir/control_mod.mod.stamp"

  "../build/modules/stell/coordinate_utilities.mod"
  "../build/modules/stell/COORDINATE_UTILITIES.mod"
  "CMakeFiles/stell.dir/coordinate_utilities.mod.stamp"

  "../build/modules/stell/cyl_flux.mod"
  "../build/modules/stell/CYL_FLUX.mod"
  "CMakeFiles/stell.dir/cyl_flux.mod.stamp"

  "../build/modules/stell/date_and_computer.mod"
  "../build/modules/stell/DATE_AND_COMPUTER.mod"
  "CMakeFiles/stell.dir/date_and_computer.mod.stamp"

  "../build/modules/stell/de_mod.mod"
  "../build/modules/stell/DE_MOD.mod"
  "CMakeFiles/stell.dir/de_mod.mod.stamp"

  "../build/modules/stell/diagnostic_cdf.mod"
  "../build/modules/stell/DIAGNOSTIC_CDF.mod"
  "CMakeFiles/stell.dir/diagnostic_cdf.mod.stamp"

  "../build/modules/stell/diagnostic_t.mod"
  "../build/modules/stell/DIAGNOSTIC_T.mod"
  "CMakeFiles/stell.dir/diagnostic_t.mod.stamp"

  "../build/modules/stell/extcurz_t.mod"
  "../build/modules/stell/EXTCURZ_T.mod"
  "CMakeFiles/stell.dir/extcurz_t.mod.stamp"

  "../build/modules/stell/ezcdf.mod"
  "../build/modules/stell/EZCDF.mod"
  "CMakeFiles/stell.dir/ezcdf.mod.stamp"

  "../build/modules/stell/ezcdf_attrib.mod"
  "../build/modules/stell/EZCDF_ATTRIB.mod"
  "CMakeFiles/stell.dir/ezcdf_attrib.mod.stamp"

  "../build/modules/stell/ezcdf_genget.mod"
  "../build/modules/stell/EZCDF_GENGET.mod"
  "CMakeFiles/stell.dir/ezcdf_genget.mod.stamp"

  "../build/modules/stell/ezcdf_genput.mod"
  "../build/modules/stell/EZCDF_GENPUT.mod"
  "CMakeFiles/stell.dir/ezcdf_genput.mod.stamp"

  "../build/modules/stell/ezcdf_inqvar.mod"
  "../build/modules/stell/EZCDF_INQVAR.mod"
  "CMakeFiles/stell.dir/ezcdf_inqvar.mod.stamp"

  "../build/modules/stell/ezcdf_opncls.mod"
  "../build/modules/stell/EZCDF_OPNCLS.mod"
  "CMakeFiles/stell.dir/ezcdf_opncls.mod.stamp"

  "../build/modules/stell/ezspline.mod"
  "../build/modules/stell/EZSPLINE.mod"
  "CMakeFiles/stell.dir/ezspline.mod.stamp"

  "../build/modules/stell/ezspline_obj.mod"
  "../build/modules/stell/EZSPLINE_OBJ.mod"
  "CMakeFiles/stell.dir/ezspline_obj.mod.stamp"

  "../build/modules/stell/ezspline_type.mod"
  "../build/modules/stell/EZSPLINE_TYPE.mod"
  "CMakeFiles/stell.dir/ezspline_type.mod.stamp"

  "../build/modules/stell/fdjac_mod.mod"
  "../build/modules/stell/FDJAC_MOD.mod"
  "CMakeFiles/stell.dir/fdjac_mod.mod.stamp"

  "../build/modules/stell/fftpack.mod"
  "../build/modules/stell/FFTPACK.mod"
  "CMakeFiles/stell.dir/fftpack.mod.stamp"

  "../build/modules/stell/file_opts.mod"
  "../build/modules/stell/FILE_OPTS.mod"
  "CMakeFiles/stell.dir/file_opts.mod.stamp"

  "../build/modules/stell/functions.mod"
  "../build/modules/stell/FUNCTIONS.mod"
  "CMakeFiles/stell.dir/functions.mod.stamp"

  "../build/modules/stell/ga_mod.mod"
  "../build/modules/stell/GA_MOD.mod"
  "CMakeFiles/stell.dir/ga_mod.mod.stamp"

  "../build/modules/stell/gade_mod.mod"
  "../build/modules/stell/GADE_MOD.mod"
  "CMakeFiles/stell.dir/gade_mod.mod.stamp"

  "../build/modules/stell/gmres_dim.mod"
  "../build/modules/stell/GMRES_DIM.mod"
  "CMakeFiles/stell.dir/gmres_dim.mod.stamp"

  "../build/modules/stell/gmres_lib.mod"
  "../build/modules/stell/GMRES_LIB.mod"
  "CMakeFiles/stell.dir/gmres_lib.mod.stamp"

  "../build/modules/stell/integration_path.mod"
  "../build/modules/stell/INTEGRATION_PATH.mod"
  "CMakeFiles/stell.dir/integration_path.mod.stamp"

  "../build/modules/stell/ipch_t.mod"
  "../build/modules/stell/IPCH_T.mod"
  "CMakeFiles/stell.dir/ipch_t.mod.stamp"

  "../build/modules/stell/line_segment.mod"
  "../build/modules/stell/LINE_SEGMENT.mod"
  "CMakeFiles/stell.dir/line_segment.mod.stamp"

  "../build/modules/stell/linear1_mod.mod"
  "../build/modules/stell/LINEAR1_MOD.mod"
  "CMakeFiles/stell.dir/linear1_mod.mod.stamp"

  "../build/modules/stell/liprec.mod"
  "../build/modules/stell/LIPREC.mod"
  "CMakeFiles/stell.dir/liprec.mod.stamp"

  "../build/modules/stell/lmpar_mod.mod"
  "../build/modules/stell/LMPAR_MOD.mod"
  "CMakeFiles/stell.dir/lmpar_mod.mod.stamp"

  "../build/modules/stell/magnetic_response.mod"
  "../build/modules/stell/MAGNETIC_RESPONSE.mod"
  "CMakeFiles/stell.dir/magnetic_response.mod.stamp"

  "../build/modules/stell/math_utilities.mod"
  "../build/modules/stell/MATH_UTILITIES.mod"
  "CMakeFiles/stell.dir/math_utilities.mod.stamp"

  "../build/modules/stell/mddc_cdf.mod"
  "../build/modules/stell/MDDC_CDF.mod"
  "CMakeFiles/stell.dir/mddc_cdf.mod.stamp"

  "../build/modules/stell/mddc_t.mod"
  "../build/modules/stell/MDDC_T.mod"
  "CMakeFiles/stell.dir/mddc_t.mod.stamp"

  "../build/modules/stell/mgrid_mod.mod"
  "../build/modules/stell/MGRID_MOD.mod"
  "CMakeFiles/stell.dir/mgrid_mod.mod.stamp"

  "../build/modules/stell/modular_coils.mod"
  "../build/modules/stell/MODULAR_COILS.mod"
  "CMakeFiles/stell.dir/modular_coils.mod.stamp"

  "../build/modules/stell/mpi_inc.mod"
  "../build/modules/stell/MPI_INC.mod"
  "CMakeFiles/stell.dir/mpi_inc.mod.stamp"

  "../build/modules/stell/mpi_params.mod"
  "../build/modules/stell/MPI_PARAMS.mod"
  "CMakeFiles/stell.dir/mpi_params.mod.stamp"

  "../build/modules/stell/optim_params.mod"
  "../build/modules/stell/OPTIM_PARAMS.mod"
  "CMakeFiles/stell.dir/optim_params.mod.stamp"

  "../build/modules/stell/packafile.mod"
  "../build/modules/stell/PACKAFILE.mod"
  "CMakeFiles/stell.dir/packafile.mod.stamp"

  "../build/modules/stell/profiler.mod"
  "../build/modules/stell/PROFILER.mod"
  "CMakeFiles/stell.dir/profiler.mod.stamp"

  "../build/modules/stell/pspline_calls.mod"
  "../build/modules/stell/PSPLINE_CALLS.mod"
  "CMakeFiles/stell.dir/pspline_calls.mod.stamp"

  "../build/modules/stell/read_boozer_mod.mod"
  "../build/modules/stell/READ_BOOZER_MOD.mod"
  "CMakeFiles/stell.dir/read_boozer_mod.mod.stamp"

  "../build/modules/stell/read_response.mod"
  "../build/modules/stell/READ_RESPONSE.mod"
  "CMakeFiles/stell.dir/read_response.mod.stamp"

  "../build/modules/stell/read_response_nompi.mod"
  "../build/modules/stell/READ_RESPONSE_NOMPI.mod"
  "CMakeFiles/stell.dir/read_response_nompi.mod.stamp"

  "../build/modules/stell/read_v3post_mod.mod"
  "../build/modules/stell/READ_V3POST_MOD.mod"
  "CMakeFiles/stell.dir/read_v3post_mod.mod.stamp"

  "../build/modules/stell/read_wout_mod.mod"
  "../build/modules/stell/READ_WOUT_MOD.mod"
  "CMakeFiles/stell.dir/read_wout_mod.mod.stamp"

  "../build/modules/stell/saddle_coils.mod"
  "../build/modules/stell/SADDLE_COILS.mod"
  "CMakeFiles/stell.dir/saddle_coils.mod.stamp"

  "../build/modules/stell/saddle_surface.mod"
  "../build/modules/stell/SADDLE_SURFACE.mod"
  "CMakeFiles/stell.dir/saddle_surface.mod.stamp"

  "../build/modules/stell/safe_open_mod.mod"
  "../build/modules/stell/SAFE_OPEN_MOD.mod"
  "CMakeFiles/stell.dir/safe_open_mod.mod.stamp"

  "../build/modules/stell/spec_kind_mod.mod"
  "../build/modules/stell/SPEC_KIND_MOD.mod"
  "CMakeFiles/stell.dir/spec_kind_mod.mod.stamp"

  "../build/modules/stell/spline1_mod.mod"
  "../build/modules/stell/SPLINE1_MOD.mod"
  "CMakeFiles/stell.dir/spline1_mod.mod.stamp"

  "../build/modules/stell/stel_constants.mod"
  "../build/modules/stell/STEL_CONSTANTS.mod"
  "CMakeFiles/stell.dir/stel_constants.mod.stamp"

  "../build/modules/stell/stel_kinds.mod"
  "../build/modules/stell/STEL_KINDS.mod"
  "CMakeFiles/stell.dir/stel_kinds.mod.stamp"

  "../build/modules/stell/sxrch_t.mod"
  "../build/modules/stell/SXRCH_T.mod"
  "CMakeFiles/stell.dir/sxrch_t.mod.stamp"

  "../build/modules/stell/system_mod.mod"
  "../build/modules/stell/SYSTEM_MOD.mod"
  "CMakeFiles/stell.dir/system_mod.mod.stamp"

  "../build/modules/stell/tf_coils.mod"
  "../build/modules/stell/TF_COILS.mod"
  "CMakeFiles/stell.dir/tf_coils.mod.stamp"

  "../build/modules/stell/thscte_t.mod"
  "../build/modules/stell/THSCTE_T.mod"
  "CMakeFiles/stell.dir/thscte_t.mod.stamp"

  "../build/modules/stell/track_mod.mod"
  "../build/modules/stell/TRACK_MOD.mod"
  "CMakeFiles/stell.dir/track_mod.mod.stamp"

  "../build/modules/stell/v3_utilities.mod"
  "../build/modules/stell/V3_UTILITIES.mod"
  "CMakeFiles/stell.dir/v3_utilities.mod.stamp"

  "../build/modules/stell/v3f_vmec_comm.mod"
  "../build/modules/stell/V3F_VMEC_COMM.mod"
  "CMakeFiles/stell.dir/v3f_vmec_comm.mod.stamp"

  "../build/modules/stell/v3post_rfun.mod"
  "../build/modules/stell/V3POST_RFUN.mod"
  "CMakeFiles/stell.dir/v3post_rfun.mod.stamp"

  "../build/modules/stell/vacfield_mod.mod"
  "../build/modules/stell/VACFIELD_MOD.mod"
  "CMakeFiles/stell.dir/vacfield_mod.mod.stamp"

  "../build/modules/stell/vcoilpts.mod"
  "../build/modules/stell/VCOILPTS.mod"
  "CMakeFiles/stell.dir/vcoilpts.mod.stamp"

  "../build/modules/stell/vertex_list.mod"
  "../build/modules/stell/VERTEX_LIST.mod"
  "CMakeFiles/stell.dir/vertex_list.mod.stamp"

  "../build/modules/stell/vf_coils.mod"
  "../build/modules/stell/VF_COILS.mod"
  "CMakeFiles/stell.dir/vf_coils.mod.stamp"

  "../build/modules/stell/virtual_casing_mod.mod"
  "../build/modules/stell/VIRTUAL_CASING_MOD.mod"
  "CMakeFiles/stell.dir/virtual_casing_mod.mod.stamp"

  "../build/modules/stell/vmec_input.mod"
  "../build/modules/stell/VMEC_INPUT.mod"
  "CMakeFiles/stell.dir/vmec_input.mod.stamp"

  "../build/modules/stell/vmec_seq.mod"
  "../build/modules/stell/VMEC_SEQ.mod"
  "CMakeFiles/stell.dir/vmec_seq.mod.stamp"

  "../build/modules/stell/vmec_utils.mod"
  "../build/modules/stell/VMEC_UTILS.mod"
  "CMakeFiles/stell.dir/vmec_utils.mod.stamp"

  "../build/modules/stell/vparams.mod"
  "../build/modules/stell/VPARAMS.mod"
  "CMakeFiles/stell.dir/vparams.mod.stamp"

  "../build/modules/stell/vsvd0.mod"
  "../build/modules/stell/VSVD0.mod"
  "CMakeFiles/stell.dir/vsvd0.mod.stamp"

  "../build/modules/stell/write_array_generic.mod"
  "../build/modules/stell/WRITE_ARRAY_GENERIC.mod"
  "CMakeFiles/stell.dir/write_array_generic.mod.stamp"

  "../build/modules/stell/write_mod.mod"
  "../build/modules/stell/WRITE_MOD.mod"
  "CMakeFiles/stell.dir/write_mod.mod.stamp"
  )

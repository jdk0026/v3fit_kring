# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

# compile Fortran with /usr/bin/gfortran
Fortran_FLAGS =   -fopenmp -DNEW_CODE -cpp -O3 -m64 -mfpmath=sse -march=native -msse4a -msse3 -J../build/modules/stell -I/usr/lib/openmpi/include -I/usr/lib/openmpi/lib -I/usr/include   

Fortran_DEFINES = -DLINUX -DMPI_OPT -DMRC -DNETCDF -DSKS


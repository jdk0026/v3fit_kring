# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk/LIBSTELL/Sources
# Build directory: /home/inst/jdk0026/v3fit/trunk/LIBSTELL/Sources
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(Coils)
subdirs(Ezcdf)
subdirs(FFTpack)
subdirs(GMRes)
subdirs(Linpack)
subdirs(Lsode)
subdirs(Miscel)
subdirs(Modules)
subdirs(Optimization)
subdirs(Pspline)
subdirs(QMRpack)
subdirs(SMPINT)
subdirs(SVDpack)
subdirs(Track)

# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:
.PHONY : .NOTPARALLEL

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/cmake

# The command to remove a file.
RM = /automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/inst/jdk0026/v3fit/trunk

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/inst/jdk0026/v3fit/trunk

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/ccmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# Special rule for the target test
test:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running tests..."
	/automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/ctest --force-new-ctest-process $(ARGS)
.PHONY : test

# Special rule for the target test
test/fast: test
.PHONY : test/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/inst/jdk0026/v3fit/trunk/CMakeFiles /home/inst/jdk0026/v3fit/trunk/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/inst/jdk0026/v3fit/trunk/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named doc

# Build rule for target.
doc: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 doc
.PHONY : doc

# fast build rule for target.
doc/fast:
	$(MAKE) -f CMakeFiles/doc.dir/build.make CMakeFiles/doc.dir/build
.PHONY : doc/fast

#=============================================================================
# Target rules for targets named stell

# Build rule for target.
stell: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 stell
.PHONY : stell

# fast build rule for target.
stell/fast:
	$(MAKE) -f LIBSTELL/CMakeFiles/stell.dir/build.make LIBSTELL/CMakeFiles/stell.dir/build
.PHONY : stell/fast

#=============================================================================
# Target rules for targets named vmec

# Build rule for target.
vmec: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 vmec
.PHONY : vmec

# fast build rule for target.
vmec/fast:
	$(MAKE) -f VMEC2000/CMakeFiles/vmec.dir/build.make VMEC2000/CMakeFiles/vmec.dir/build
.PHONY : vmec/fast

#=============================================================================
# Target rules for targets named xvmec

# Build rule for target.
xvmec: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xvmec
.PHONY : xvmec

# fast build rule for target.
xvmec/fast:
	$(MAKE) -f VMEC2000/CMakeFiles/xvmec.dir/build.make VMEC2000/CMakeFiles/xvmec.dir/build
.PHONY : xvmec/fast

#=============================================================================
# Target rules for targets named xpoincare

# Build rule for target.
xpoincare: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xpoincare
.PHONY : xpoincare

# fast build rule for target.
xpoincare/fast:
	$(MAKE) -f POINCARE/CMakeFiles/xpoincare.dir/build.make POINCARE/CMakeFiles/xpoincare.dir/build
.PHONY : xpoincare/fast

#=============================================================================
# Target rules for targets named siesta

# Build rule for target.
siesta: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 siesta
.PHONY : siesta

# fast build rule for target.
siesta/fast:
	$(MAKE) -f SIESTA/CMakeFiles/siesta.dir/build.make SIESTA/CMakeFiles/siesta.dir/build
.PHONY : siesta/fast

#=============================================================================
# Target rules for targets named xsiesta

# Build rule for target.
xsiesta: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xsiesta
.PHONY : xsiesta

# fast build rule for target.
xsiesta/fast:
	$(MAKE) -f SIESTA/CMakeFiles/xsiesta.dir/build.make SIESTA/CMakeFiles/xsiesta.dir/build
.PHONY : xsiesta/fast

#=============================================================================
# Target rules for targets named xv3fit

# Build rule for target.
xv3fit: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xv3fit
.PHONY : xv3fit

# fast build rule for target.
xv3fit/fast:
	$(MAKE) -f V3FITA/CMakeFiles/xv3fit.dir/build.make V3FITA/CMakeFiles/xv3fit.dir/build
.PHONY : xv3fit/fast

#=============================================================================
# Target rules for targets named mgrid

# Build rule for target.
mgrid: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 mgrid
.PHONY : mgrid

# fast build rule for target.
mgrid/fast:
	$(MAKE) -f MAKEGRID/CMakeFiles/mgrid.dir/build.make MAKEGRID/CMakeFiles/mgrid.dir/build
.PHONY : mgrid/fast

#=============================================================================
# Target rules for targets named xv3rfun

# Build rule for target.
xv3rfun: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xv3rfun
.PHONY : xv3rfun

# fast build rule for target.
xv3rfun/fast:
	$(MAKE) -f V3RFUN/CMakeFiles/xv3rfun.dir/build.make V3RFUN/CMakeFiles/xv3rfun.dir/build
.PHONY : xv3rfun/fast

#=============================================================================
# Target rules for targets named xbooz_xform

# Build rule for target.
xbooz_xform: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xbooz_xform
.PHONY : xbooz_xform

# fast build rule for target.
xbooz_xform/fast:
	$(MAKE) -f BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/build.make BOOZ_XFORM/CMakeFiles/xbooz_xform.dir/build
.PHONY : xbooz_xform/fast

#=============================================================================
# Target rules for targets named xwout_converter

# Build rule for target.
xwout_converter: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xwout_converter
.PHONY : xwout_converter

# fast build rule for target.
xwout_converter/fast:
	$(MAKE) -f WOUT_CONVERTER/CMakeFiles/xwout_converter.dir/build.make WOUT_CONVERTER/CMakeFiles/xwout_converter.dir/build
.PHONY : xwout_converter/fast

#=============================================================================
# Target rules for targets named xbootsj

# Build rule for target.
xbootsj: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xbootsj
.PHONY : xbootsj

# fast build rule for target.
xbootsj/fast:
	$(MAKE) -f BOOTSJ/CMakeFiles/xbootsj.dir/build.make BOOTSJ/CMakeFiles/xbootsj.dir/build
.PHONY : xbootsj/fast

#=============================================================================
# Target rules for targets named xdescur

# Build rule for target.
xdescur: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xdescur
.PHONY : xdescur

# fast build rule for target.
xdescur/fast:
	$(MAKE) -f DESCUR/CMakeFiles/xdescur.dir/build.make DESCUR/CMakeFiles/xdescur.dir/build
.PHONY : xdescur/fast

#=============================================================================
# Target rules for targets named bmw

# Build rule for target.
bmw: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 bmw
.PHONY : bmw

# fast build rule for target.
bmw/fast:
	$(MAKE) -f BMW/CMakeFiles/bmw.dir/build.make BMW/CMakeFiles/bmw.dir/build
.PHONY : bmw/fast

#=============================================================================
# Target rules for targets named xbmw

# Build rule for target.
xbmw: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 xbmw
.PHONY : xbmw

# fast build rule for target.
xbmw/fast:
	$(MAKE) -f BMW/CMakeFiles/xbmw.dir/build.make BMW/CMakeFiles/xbmw.dir/build
.PHONY : xbmw/fast

#=============================================================================
# Target rules for targets named lgrid

# Build rule for target.
lgrid: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 lgrid
.PHONY : lgrid

# fast build rule for target.
lgrid/fast:
	$(MAKE) -f LGRID/CMakeFiles/lgrid.dir/build.make LGRID/CMakeFiles/lgrid.dir/build
.PHONY : lgrid/fast

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... doc"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... test"
	@echo "... stell"
	@echo "... vmec"
	@echo "... xvmec"
	@echo "... xpoincare"
	@echo "... siesta"
	@echo "... xsiesta"
	@echo "... xv3fit"
	@echo "... mgrid"
	@echo "... xv3rfun"
	@echo "... xbooz_xform"
	@echo "... xwout_converter"
	@echo "... xbootsj"
	@echo "... xdescur"
	@echo "... bmw"
	@echo "... xbmw"
	@echo "... lgrid"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system


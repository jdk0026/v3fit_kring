# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/bspline.f90" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/bspline.f90.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/enforce_periodicity.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/enforce_periodicity.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/jacobian.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/jacobian.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/lsode_quantities.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/lsode_quantities.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/poincare.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/poincare.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/reset_coords.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/reset_coords.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/rt_hand_side_dphi.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/rt_hand_side_dphi.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/rt_hand_side_dtheta.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/rt_hand_side_dtheta.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/spline3d_fit_coefs.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/spline3d_fit_coefs.f.o"
  "/home/inst/jdk0026/v3fit/trunk/POINCARE/Sources/spline_fields.f" "/home/inst/jdk0026/v3fit/trunk/POINCARE/CMakeFiles/xpoincare.dir/Sources/spline_fields.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/inst/jdk0026/v3fit/trunk/LIBSTELL/CMakeFiles/stell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules/xpoincare")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

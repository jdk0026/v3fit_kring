file(REMOVE_RECURSE
  "CMakeFiles/xpoincare.dir/Sources/bspline.f90.o"
  "CMakeFiles/xpoincare.dir/Sources/enforce_periodicity.f.o"
  "CMakeFiles/xpoincare.dir/Sources/jacobian.f.o"
  "CMakeFiles/xpoincare.dir/Sources/lsode_quantities.f.o"
  "CMakeFiles/xpoincare.dir/Sources/poincare.f.o"
  "CMakeFiles/xpoincare.dir/Sources/reset_coords.f.o"
  "CMakeFiles/xpoincare.dir/Sources/rt_hand_side_dphi.f.o"
  "CMakeFiles/xpoincare.dir/Sources/rt_hand_side_dtheta.f.o"
  "CMakeFiles/xpoincare.dir/Sources/spline_fields.f.o"
  "CMakeFiles/xpoincare.dir/Sources/spline3d_fit_coefs.f.o"
  "../build/bin/xpoincare.pdb"
  "../build/bin/xpoincare"
)

# Per-language clean rules from dependency scanning.
foreach(lang Fortran)
  include(CMakeFiles/xpoincare.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/xpoincare/bspline.mod"
  "../build/modules/xpoincare/BSPLINE.mod"
  "CMakeFiles/xpoincare.dir/bspline.mod.stamp"

  "../build/modules/xpoincare/lsode_quantities.mod"
  "../build/modules/xpoincare/LSODE_QUANTITIES.mod"
  "CMakeFiles/xpoincare.dir/lsode_quantities.mod.stamp"

  "../build/modules/xpoincare/spline3d_fit_coefs.mod"
  "../build/modules/xpoincare/SPLINE3D_FIT_COEFS.mod"
  "CMakeFiles/xpoincare.dir/spline3d_fit_coefs.mod.stamp"
  )

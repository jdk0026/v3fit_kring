# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/siesta/assert_mod.mod"
  "../build/modules/siesta/ASSERT_MOD.mod"
  "CMakeFiles/siesta.dir/assert_mod.mod.stamp"

  "../build/modules/siesta/bhtobf.mod"
  "../build/modules/siesta/BHTOBF.mod"
  "CMakeFiles/siesta.dir/bhtobf.mod.stamp"

  "../build/modules/siesta/blocktridiagonalsolver_s.mod"
  "../build/modules/siesta/BLOCKTRIDIAGONALSOLVER_S.mod"
  "CMakeFiles/siesta.dir/blocktridiagonalsolver_s.mod.stamp"

  "../build/modules/siesta/descriptor_mod.mod"
  "../build/modules/siesta/DESCRIPTOR_MOD.mod"
  "CMakeFiles/siesta.dir/descriptor_mod.mod.stamp"

  "../build/modules/siesta/diagnostics_mod.mod"
  "../build/modules/siesta/DIAGNOSTICS_MOD.mod"
  "CMakeFiles/siesta.dir/diagnostics_mod.mod.stamp"

  "../build/modules/siesta/dumping_mod.mod"
  "../build/modules/siesta/DUMPING_MOD.mod"
  "CMakeFiles/siesta.dir/dumping_mod.mod.stamp"

  "../build/modules/siesta/evolution.mod"
  "../build/modules/siesta/EVOLUTION.mod"
  "CMakeFiles/siesta.dir/evolution.mod.stamp"

  "../build/modules/siesta/f1dim_mod.mod"
  "../build/modules/siesta/F1DIM_MOD.mod"
  "CMakeFiles/siesta.dir/f1dim_mod.mod.stamp"

  "../build/modules/siesta/fourier.mod"
  "../build/modules/siesta/FOURIER.mod"
  "CMakeFiles/siesta.dir/fourier.mod.stamp"

  "../build/modules/siesta/gmres.mod"
  "../build/modules/siesta/GMRES.mod"
  "CMakeFiles/siesta.dir/gmres.mod.stamp"

  "../build/modules/siesta/hessian.mod"
  "../build/modules/siesta/HESSIAN.mod"
  "CMakeFiles/siesta.dir/hessian.mod.stamp"

  "../build/modules/siesta/island_params.mod"
  "../build/modules/siesta/ISLAND_PARAMS.mod"
  "CMakeFiles/siesta.dir/island_params.mod.stamp"

  "../build/modules/siesta/metrics.mod"
  "../build/modules/siesta/METRICS.mod"
  "CMakeFiles/siesta.dir/metrics.mod.stamp"

  "../build/modules/siesta/nscalingtools.mod"
  "../build/modules/siesta/NSCALINGTOOLS.mod"
  "CMakeFiles/siesta.dir/nscalingtools.mod.stamp"

  "../build/modules/siesta/perturbation.mod"
  "../build/modules/siesta/PERTURBATION.mod"
  "CMakeFiles/siesta.dir/perturbation.mod.stamp"

  "../build/modules/siesta/prof_mod.mod"
  "../build/modules/siesta/PROF_MOD.mod"
  "CMakeFiles/siesta.dir/prof_mod.mod.stamp"

  "../build/modules/siesta/ptrd_mod.mod"
  "../build/modules/siesta/PTRD_MOD.mod"
  "CMakeFiles/siesta.dir/ptrd_mod.mod.stamp"

  "../build/modules/siesta/quantities.mod"
  "../build/modules/siesta/QUANTITIES.mod"
  "CMakeFiles/siesta.dir/quantities.mod.stamp"

  "../build/modules/siesta/restart_mod.mod"
  "../build/modules/siesta/RESTART_MOD.mod"
  "CMakeFiles/siesta.dir/restart_mod.mod.stamp"

  "../build/modules/siesta/shared_data.mod"
  "../build/modules/siesta/SHARED_DATA.mod"
  "CMakeFiles/siesta.dir/shared_data.mod.stamp"

  "../build/modules/siesta/shared_functions.mod"
  "../build/modules/siesta/SHARED_FUNCTIONS.mod"
  "CMakeFiles/siesta.dir/shared_functions.mod.stamp"

  "../build/modules/siesta/siesta_bfield.mod"
  "../build/modules/siesta/SIESTA_BFIELD.mod"
  "CMakeFiles/siesta.dir/siesta_bfield.mod.stamp"

  "../build/modules/siesta/siesta_currents.mod"
  "../build/modules/siesta/SIESTA_CURRENTS.mod"
  "CMakeFiles/siesta.dir/siesta_currents.mod.stamp"

  "../build/modules/siesta/siesta_displacement.mod"
  "../build/modules/siesta/SIESTA_DISPLACEMENT.mod"
  "CMakeFiles/siesta.dir/siesta_displacement.mod.stamp"

  "../build/modules/siesta/siesta_error.mod"
  "../build/modules/siesta/SIESTA_ERROR.mod"
  "CMakeFiles/siesta.dir/siesta_error.mod.stamp"

  "../build/modules/siesta/siesta_force.mod"
  "../build/modules/siesta/SIESTA_FORCE.mod"
  "CMakeFiles/siesta.dir/siesta_force.mod.stamp"

  "../build/modules/siesta/siesta_init.mod"
  "../build/modules/siesta/SIESTA_INIT.mod"
  "CMakeFiles/siesta.dir/siesta_init.mod.stamp"

  "../build/modules/siesta/siesta_namelist.mod"
  "../build/modules/siesta/SIESTA_NAMELIST.mod"
  "CMakeFiles/siesta.dir/siesta_namelist.mod.stamp"

  "../build/modules/siesta/siesta_pressure.mod"
  "../build/modules/siesta/SIESTA_PRESSURE.mod"
  "CMakeFiles/siesta.dir/siesta_pressure.mod.stamp"

  "../build/modules/siesta/siesta_run.mod"
  "../build/modules/siesta/SIESTA_RUN.mod"
  "CMakeFiles/siesta.dir/siesta_run.mod.stamp"

  "../build/modules/siesta/siesta_state.mod"
  "../build/modules/siesta/SIESTA_STATE.mod"
  "CMakeFiles/siesta.dir/siesta_state.mod.stamp"

  "../build/modules/siesta/timer_mod.mod"
  "../build/modules/siesta/TIMER_MOD.mod"
  "CMakeFiles/siesta.dir/timer_mod.mod.stamp"

  "../build/modules/siesta/utilities.mod"
  "../build/modules/siesta/UTILITIES.mod"
  "CMakeFiles/siesta.dir/utilities.mod.stamp"

  "../build/modules/siesta/vmec_info.mod"
  "../build/modules/siesta/VMEC_INFO.mod"
  "CMakeFiles/siesta.dir/vmec_info.mod.stamp"
  )

# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/cmake

# The command to remove a file.
RM = /automnt/opt/cmake/cmake-3.0.0-Linux-i386/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/inst/jdk0026/v3fit/trunk

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/inst/jdk0026/v3fit/trunk

# Include any dependencies generated for this target.
include SIESTA/CMakeFiles/xsiesta.dir/depend.make

# Include the progress variables for this target.
include SIESTA/CMakeFiles/xsiesta.dir/progress.make

# Include the compile flags for this target's objects.
include SIESTA/CMakeFiles/xsiesta.dir/flags.make

SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o: SIESTA/CMakeFiles/xsiesta.dir/flags.make
SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o: SIESTA/Sources/siesta.f90
	$(CMAKE_COMMAND) -E cmake_progress_report /home/inst/jdk0026/v3fit/trunk/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building Fortran object SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o"
	cd /home/inst/jdk0026/v3fit/trunk/SIESTA && /usr/bin/gfortran  $(Fortran_DEFINES) $(Fortran_FLAGS) -c /home/inst/jdk0026/v3fit/trunk/SIESTA/Sources/siesta.f90 -o CMakeFiles/xsiesta.dir/Sources/siesta.f90.o

SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.requires:
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.requires

SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.provides: SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.requires
	$(MAKE) -f SIESTA/CMakeFiles/xsiesta.dir/build.make SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.provides.build
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.provides

SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.provides.build: SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o

# Object files for target xsiesta
xsiesta_OBJECTS = \
"CMakeFiles/xsiesta.dir/Sources/siesta.f90.o"

# External object files for target xsiesta
xsiesta_EXTERNAL_OBJECTS =

build/bin/xsiesta: SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o
build/bin/xsiesta: SIESTA/CMakeFiles/xsiesta.dir/build.make
build/bin/xsiesta: build/lib/libsiesta.a
build/bin/xsiesta: build/lib/libstell.a
build/bin/xsiesta: /usr/lib/x86_64-linux-gnu/libnetcdff.so
build/bin/xsiesta: /usr/lib/x86_64-linux-gnu/libnetcdf.so
build/bin/xsiesta: /usr/lib/libscalapack-openmpi.so.1
build/bin/xsiesta: /usr/lib/libblacs-openmpi.so.1
build/bin/xsiesta: /usr/lib/libblacsCinit-openmpi.so.1
build/bin/xsiesta: /usr/lib/libblacsF77init-openmpi.so.1
build/bin/xsiesta: /usr/lib/libblas.so
build/bin/xsiesta: /usr/lib/liblapack.so
build/bin/xsiesta: /usr/lib/libblas.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_usempif08.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_usempi_ignore_tkr.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_mpifh.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi.so
build/bin/xsiesta: /usr/lib/liblapack.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_usempif08.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_usempi_ignore_tkr.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi_mpifh.so
build/bin/xsiesta: /usr/lib/openmpi/lib/libmpi.so
build/bin/xsiesta: SIESTA/CMakeFiles/xsiesta.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking Fortran executable ../build/bin/xsiesta"
	cd /home/inst/jdk0026/v3fit/trunk/SIESTA && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/xsiesta.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
SIESTA/CMakeFiles/xsiesta.dir/build: build/bin/xsiesta
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/build

SIESTA/CMakeFiles/xsiesta.dir/requires: SIESTA/CMakeFiles/xsiesta.dir/Sources/siesta.f90.o.requires
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/requires

SIESTA/CMakeFiles/xsiesta.dir/clean:
	cd /home/inst/jdk0026/v3fit/trunk/SIESTA && $(CMAKE_COMMAND) -P CMakeFiles/xsiesta.dir/cmake_clean.cmake
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/clean

SIESTA/CMakeFiles/xsiesta.dir/depend:
	cd /home/inst/jdk0026/v3fit/trunk && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/inst/jdk0026/v3fit/trunk /home/inst/jdk0026/v3fit/trunk/SIESTA /home/inst/jdk0026/v3fit/trunk /home/inst/jdk0026/v3fit/trunk/SIESTA /home/inst/jdk0026/v3fit/trunk/SIESTA/CMakeFiles/xsiesta.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : SIESTA/CMakeFiles/xsiesta.dir/depend


!>  \brief Module contained subroutines and functions updating MHD forces and Wmhd
!!  \author S. P. Hirshman
!!  \date July, 2014
      MODULE shared_functions
      USE v3_utilities, ONLY: assert
      USE shared_data
      USE descriptor_mod, ONLY: INHESSIAN, nprocs, iam, SIESTA_COMM
      USE island_params, ns=>ns_i, hs=>hs_i
      USE hessian, ONLY: apply_precond, l_Compute_Hessian, apply_colscale
      USE quantities, ONLY: wb, wp
      USE timer_mod, ONLY: time_init_state, time_funci
      USE nscalingtools, ONLY: startglobrow, endglobrow, MPI_ERR,       &
                               PARSOLVER
      USE mpi_inc
      USE GMRES_LIB, ONLY: Truncate
      
      IMPLICIT NONE

      INTEGER, PRIVATE :: nsmin, nsmax
      
      CONTAINS

!>  \brief Parallel/Serial routine to compute forces for perturbed state.
!!  \author S. P. Hirshman, R. Sanchez, S. K. Seal
      SUBROUTINE funct_island
!-----------------------------------------------
!     PARALLEL (in N rows, PARSOLVER=T)
!     SERIAL   (PARSOLVER=F, startglobrow=1, endglobrow=ns)
!-----------------------------------------------
      USE siesta_bfield, ONLY: update_bfield
      USE siesta_pressure, ONLY: update_pres
      USE siesta_force, ONLY: update_force
      USE siesta_displacement, ONLY: update_upperv
      USE siesta_init, ONLY: init_state
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      REAL(dp) :: ton, toff, skston, skstoff
      LOGICAL  :: ltype
!-----------------------------------------------
      CALL second0(ton)
      nsmin=MAX(1,startglobrow); nsmax=MIN(endglobrow,ns)

      IF (INHESSIAN) THEN
         in_hess_nfunct=in_hess_nfunct+1
      ELSE
         out_hess_nfunct=out_hess_nfunct+1
      END IF

      l_update_state = .FALSE.
      IF (l_init_state) THEN
        CALL second0(skston)
        CALL init_state(.FALSE.)
        CALL second0(skstoff)
        time_init_state=time_init_state+(skstoff-skston)
        l_init_state = .FALSE.
      END IF

      CALL update_upperv
      CALL update_bfield (.FALSE.)
      CALL update_pres
      CALL update_force

      CALL ASSERT(gamma.NE.one,'SIESTA REQUIRES gamma != 1')
      wtotal = wb + wp/(gamma-1)
      IF (wtotal0 == -1) wtotal0 = wtotal
      
      gc = gnorm_i*gc

!     CALLED FROM GMRES: ADD BACK INITIAL FORCE gc0 or STORE UNPRECONDITIONED FORCES
      IF (ALLOCATED(gc0) .AND. (l_getfsq .OR. l_conjgrad) .AND.         &
         l_linearize .AND. .NOT.l_Compute_Hessian) gc = gc+gc0

      CALL ASSERT(.NOT.(l_getfsq.AND.inhessian),                        &
                  'l_getfsq must be set to FALSE in Hessian')

      IF (l_getfsq) THEN
! COMPUTE PRECONDITIONED, VOLUME-AVERAGED FORCE fsq_total 
         CALL GetFSQ(fsq_total)

!COMPUTE UN-PRECONDITIONED, VOLUME-AVERAGED FORCE fsq_total1
         IF (ANY(col_scale .NE. one)) THEN
            col_scale(:,:,:,nsmin:nsmax) = one/col_scale(:,:,:,nsmin:nsmax)
            CALL Apply_ColScale(gc, col_scale, nsmin, nsmax)

            CALL GetFSQ(fsq_total1)

            col_scale(:,:,:,nsmin:nsmax) = one/col_scale(:,:,:,nsmin:nsmax)            
            CALL Apply_ColScale(gc, col_scale, nsmin, nsmax)
         ELSE
            fsq_total1 = fsq_total
         END IF
      END IF

!
!     GMRES handles preconditioning itself: do NOT apply
!     Do not precondition when Hessian is being computed
      IF (l_ApplyPrecon) CALL apply_precond(gc)

      CALL second0(toff)
      time_funci = time_funci+(toff-ton)

      END SUBROUTINE funct_island


      SUBROUTINE GetFSQ(fsqout)
      USE quantities, ONLY: fsubsmncf, fsubumnsf, fsubvmnsf,            &
                            fsupsmncf, fsupumnsf, fsupvmnsf,            &
                            fsubsmnsf, fsubumncf, fsubvmncf,            &
                            fsupsmnsf, fsupumncf, fsupvmncf,            &
                            toupper_forces
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      REAL(dp), INTENT(OUT) :: fsqout
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      REAL(dp) :: stmp(3), rtmp(3)
      INTEGER  :: js
!-----------------------------------------------
      stmp(1) = SUM(fsubsmncf(:,:,nsmin:nsmax)**2)
      stmp(2) = SUM(fsubumnsf(:,:,nsmin:nsmax)**2)
      stmp(3) = SUM(fsubvmnsf(:,:,nsmin:nsmax)**2)
      IF (lasym) THEN
         stmp(1) = stmp(1) + SUM(fsubsmnsf(:,:,nsmin:nsmax)**2)
         stmp(2) = stmp(2) + SUM(fsubumncf(:,:,nsmin:nsmax)**2)
         stmp(3) = stmp(3) + SUM(fsubvmncf(:,:,nsmin:nsmax)**2)
      END IF
         
#if defined(MPI_OPT)         
      IF (PARSOLVER) THEN
         CALL MPI_ALLREDUCE(stmp,rtmp,3,MPI_REAL8,MPI_SUM,              &
                            SIESTA_COMM,MPI_ERR)
         stmp = rtmp
      END IF
#endif         
      fsqvs=hs*stmp(1); fsqvu=hs*stmp(2); fsqvv=hs*stmp(3)

      CALL toupper_forces

!VOLUME-AVERAGE |F|**2
      stmp(1) = 0
      DO js = nsmin,nsmax
         stmp(1) = stmp(1) + vp_f(js)*SUM(                              &
                   fsupsmncf(:,:,js)*fsubsmncf(:,:,js)                  & 
                 + fsupumnsf(:,:,js)*fsubumnsf(:,:,js)                  &
                 + fsupvmnsf(:,:,js)*fsubvmnsf(:,:,js))
         IF (lasym) THEN
         stmp(1) = stmp(1) + vp_f(js)*SUM(                              &
                   fsupsmnsf(:,:,js)*fsubsmnsf(:,:,js)                  &
                 + fsupumncf(:,:,js)*fsubumncf(:,:,js)                  &
                 + fsupvmncf(:,:,js)*fsubvmncf(:,:,js))
         END IF
      END DO
      stmp(2) = SUM(vp_f(nsmin:nsmax))
#if defined(MPI_OPT)         
      IF (PARSOLVER) THEN
         CALL MPI_ALLREDUCE(stmp,rtmp,2,MPI_REAL8,MPI_SUM,              &
                            SIESTA_COMM,MPI_ERR)
         stmp = rtmp
      END IF
#endif
      fsqout = stmp(1)/stmp(2)
      
      
      END SUBROUTINE GetFSQ


!>   \brief Returns perturbed MHD+KINETIC energy
!!   \author S. P. Hirshman, R. Sanchez
      FUNCTION getwmhd (p)
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      REAL(dp), INTENT(in) :: p(neqs)           !<displacement vector
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      REAL(dp) :: getwmhd
!-----------------------------------------------
        
      xc = p
      l_init_state = .TRUE.
      l_linearize  = .FALSE.
      l_getwmhd    = .TRUE.
      l_getfsq     = .FALSE.

      CALL funct_island

      l_getwmhd    = .FALSE.

      getwmhd = wtotal

      RETURN

      END FUNCTION getwmhd 
      

!>    \brief Performs line search along xc vector for minimum fsq_total1
!!    \author S. P. Hirshman (2016)
      SUBROUTINE LineSearch(xcmin, fsq_min)
      USE siesta_namelist, ONLY: ftol
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      REAL(dp), INTENT(INOUT) :: xcmin(neqs)
      REAL(dp), INTENT(INOUT) :: fsq_min
!-----------------------------------------------
      REAL(dp) :: facmin
      INTEGER  :: iter, j
      LOGICAL  :: lwrite
!-----------------------------------------------
      facmin = 2
      l_init_state = .FALSE.
      l_getfsq = .TRUE.
      
      lwrite = (iam .EQ. 0 .and. lverbose) 
      IF (lwrite) PRINT 90
 90   FORMAT(/,1x,'LINE SEARCH - SCAN ||X|| FOR MIN FSQ_NL',/,1x,15('-'), &
           /,1x,'ITER',7x,'FSQ_NL',10x,'||X||',9x,'MAX|X|',10x,'FAC')

      xc = facmin*xcmin
      iter = 0
      fsq_min = HUGE(fsq_min)

      DO j = 1, 100
         CALL funct_island
         IF (fsq_total1 .LT. fsq_min) THEN
            xcmin = xc
            IF (fsq_total1 .GT. 0.98_dp*fsq_min) iter=iter+1
            IF (fsq_total1 .LT. 0.85_dp*fsq_min) iter = 0
            fsq_min = fsq_total1
         ELSE IF (j .GT. 4) THEN
            iter = iter+1
         END IF
         IF (iam .EQ. 0 .AND. lverbose) THEN
            PRINT 100, j, fsq_total1, SQRT(SUM(xc*xc)),                 &
                       MAXVAL(ABS(xc)), facmin
            facmin = facmin/SQRT2
         END IF
         IF (iter.GT.2 .OR. fsq_total1.LE.ftol) EXIT
         xc = xc/SQRT2
      END DO

 100  FORMAT(i5,4(3x,1pe12.3))
 
      END SUBROUTINE LineSearch


      END MODULE shared_functions

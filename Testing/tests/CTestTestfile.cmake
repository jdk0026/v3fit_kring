# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests
# Build directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(unit_tests)
subdirs(equilibrium_task_test)
subdirs(v3post_task_test)
subdirs(te_pressure_test)
subdirs(ne_pressure_test)
subdirs(reconstruction_task_test)
subdirs(reconstruction_task_seg_step_test)
subdirs(reconstruction_task_sl_step_test)
subdirs(param_constraints_test)
subdirs(diagnostic_rotation_test)
subdirs(v3post_lasym_test)
subdirs(z_offset_test)
subdirs(equilibrium_reset_test)
subdirs(equilibrium_reset_free_boundary_test)
subdirs(multigrid_test)
subdirs(aphi_test)
subdirs(lasym_boundary)
subdirs(preconditioner_test)

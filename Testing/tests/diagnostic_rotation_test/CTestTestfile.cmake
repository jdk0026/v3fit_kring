# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test
# Build directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(diagnostic_rotation_test_mdsig "/home/inst/jdk0026/v3fit/trunk/build/bin/xv3rfun" "v3rfun.in")
add_test(diagnostic_rotation_test "/home/inst/jdk0026/v3fit/trunk/build/bin/xv3fit" "input.test.v3fit" "-para=-1" "-mpi=auto")
add_test(diagnostic_rotation_test_phi_offset "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_param_value.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test/recout.input.test.v3fit" "--p_type=phi_offset" "--value=0.157" "--range=0.0002")
add_test(diagnostic_rotation_test_sem_totals "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_sem_total.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test/recout.input.test.v3fit")
add_test(diagnostic_rotation_test_check_reconstruction_correlation_matrix "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_correlation_matrix.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test/recout.input.test.v3fit" "--param=Reconstruction")
add_test(diagnostic_rotation_test_check_sxrem_model "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_signal_value.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test/recout.input.test.v3fit" "--s_name=sxr1-005" "--attribute=model" "--value=5.0E-1" "--range=1.0E-1")
add_test(diagnostic_rotation_test_check_sxrem_model_2 "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_signal_value.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/diagnostic_rotation_test/recout.input.test.v3fit" "--s_name=sxr2-006" "--attribute=model" "--value=1.0" "--range=1.0E-1")

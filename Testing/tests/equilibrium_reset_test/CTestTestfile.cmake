# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests/equilibrium_reset_test
# Build directory: /home/inst/jdk0026/v3fit/trunk/Testing/tests/equilibrium_reset_test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(equilibrium_reset_test "/home/inst/jdk0026/v3fit/trunk/build/bin/xv3fit" "input.test.v3fit" "-para=-1")
add_test(equilibrium_reset_test_jacobian_iter "/home/inst/jdk0026/anaconda3/bin/python" "/home/inst/jdk0026/v3fit/trunk/Testing/test_utilities/check_jacobian_iter.py" "--file=/home/inst/jdk0026/v3fit/trunk/Testing/tests/equilibrium_reset_test/runlog.input.test.v3fit")

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/inst/jdk0026/v3fit/trunk/V3RFUN/Sources/diagnostic_dot.f" "/home/inst/jdk0026/v3fit/trunk/V3RFUN/CMakeFiles/xv3rfun.dir/Sources/diagnostic_dot.f.o"
  "/home/inst/jdk0026/v3fit/trunk/V3RFUN/Sources/v3rfun.f" "/home/inst/jdk0026/v3fit/trunk/V3RFUN/CMakeFiles/xv3rfun.dir/Sources/v3rfun.f.o"
  "/home/inst/jdk0026/v3fit/trunk/V3RFUN/Sources/v3rfun_context.f" "/home/inst/jdk0026/v3fit/trunk/V3RFUN/CMakeFiles/xv3rfun.dir/Sources/v3rfun_context.f.o"
  "/home/inst/jdk0026/v3fit/trunk/V3RFUN/Sources/v3rfun_input.f" "/home/inst/jdk0026/v3fit/trunk/V3RFUN/CMakeFiles/xv3rfun.dir/Sources/v3rfun_input.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "LINUX"
  "MPI_OPT"
  "MRC"
  "NETCDF"
  "SKS"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/inst/jdk0026/v3fit/trunk/LIBSTELL/CMakeFiles/stell.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "/home/inst/jdk0026/v3fit/trunk/build/modules/xv3rfun")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "build/modules/stell"
  "/usr/lib/openmpi/include"
  "/usr/lib/openmpi/lib"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

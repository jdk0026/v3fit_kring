# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/xv3rfun/diagnostic_dot.mod"
  "../build/modules/xv3rfun/DIAGNOSTIC_DOT.mod"
  "CMakeFiles/xv3rfun.dir/diagnostic_dot.mod.stamp"

  "../build/modules/xv3rfun/v3rfun_context.mod"
  "../build/modules/xv3rfun/V3RFUN_CONTEXT.mod"
  "CMakeFiles/xv3rfun.dir/v3rfun_context.mod.stamp"

  "../build/modules/xv3rfun/v3rfun_input.mod"
  "../build/modules/xv3rfun/V3RFUN_INPUT.mod"
  "CMakeFiles/xv3rfun.dir/v3rfun_input.mod.stamp"
  )

# Remove fortran modules provided by this target.
FILE(REMOVE
  "../build/modules/vmec/angle_constraints.mod"
  "../build/modules/vmec/ANGLE_CONSTRAINTS.mod"
  "CMakeFiles/vmec.dir/angle_constraints.mod.stamp"

  "../build/modules/vmec/blocktridiagonalsolver.mod"
  "../build/modules/vmec/BLOCKTRIDIAGONALSOLVER.mod"
  "CMakeFiles/vmec.dir/blocktridiagonalsolver.mod.stamp"

  "../build/modules/vmec/blocktridiagonalsolver_bst.mod"
  "../build/modules/vmec/BLOCKTRIDIAGONALSOLVER_BST.mod"
  "CMakeFiles/vmec.dir/blocktridiagonalsolver_bst.mod.stamp"

  "../build/modules/vmec/csplinx.mod"
  "../build/modules/vmec/CSPLINX.mod"
  "CMakeFiles/vmec.dir/csplinx.mod.stamp"

  "../build/modules/vmec/directaccess.mod"
  "../build/modules/vmec/DIRECTACCESS.mod"
  "CMakeFiles/vmec.dir/directaccess.mod.stamp"

  "../build/modules/vmec/dump_output.mod"
  "../build/modules/vmec/DUMP_OUTPUT.mod"
  "CMakeFiles/vmec.dir/dump_output.mod.stamp"

  "../build/modules/vmec/fbal.mod"
  "../build/modules/vmec/FBAL.mod"
  "CMakeFiles/vmec.dir/fbal.mod.stamp"

  "../build/modules/vmec/gmres_mod.mod"
  "../build/modules/vmec/GMRES_MOD.mod"
  "CMakeFiles/vmec.dir/gmres_mod.mod.stamp"

  "../build/modules/vmec/init_geometry.mod"
  "../build/modules/vmec/INIT_GEOMETRY.mod"
  "CMakeFiles/vmec.dir/init_geometry.mod.stamp"

  "../build/modules/vmec/parallel_include_module.mod"
  "../build/modules/vmec/PARALLEL_INCLUDE_MODULE.mod"
  "CMakeFiles/vmec.dir/parallel_include_module.mod.stamp"

  "../build/modules/vmec/parallel_vmec_module.mod"
  "../build/modules/vmec/PARALLEL_VMEC_MODULE.mod"
  "CMakeFiles/vmec.dir/parallel_vmec_module.mod.stamp"

  "../build/modules/vmec/precon2d.mod"
  "../build/modules/vmec/PRECON2D.mod"
  "CMakeFiles/vmec.dir/precon2d.mod.stamp"

  "../build/modules/vmec/realspace.mod"
  "../build/modules/vmec/REALSPACE.mod"
  "CMakeFiles/vmec.dir/realspace.mod.stamp"

  "../build/modules/vmec/timer_sub.mod"
  "../build/modules/vmec/TIMER_SUB.mod"
  "CMakeFiles/vmec.dir/timer_sub.mod.stamp"

  "../build/modules/vmec/tomnsp_mod.mod"
  "../build/modules/vmec/TOMNSP_MOD.mod"
  "CMakeFiles/vmec.dir/tomnsp_mod.mod.stamp"

  "../build/modules/vmec/totzsp_mod.mod"
  "../build/modules/vmec/TOTZSP_MOD.mod"
  "CMakeFiles/vmec.dir/totzsp_mod.mod.stamp"

  "../build/modules/vmec/vac_persistent.mod"
  "../build/modules/vmec/VAC_PERSISTENT.mod"
  "CMakeFiles/vmec.dir/vac_persistent.mod.stamp"

  "../build/modules/vmec/vacmod.mod"
  "../build/modules/vmec/VACMOD.mod"
  "CMakeFiles/vmec.dir/vacmod.mod.stamp"

  "../build/modules/vmec/vacmod0.mod"
  "../build/modules/vmec/VACMOD0.mod"
  "CMakeFiles/vmec.dir/vacmod0.mod.stamp"

  "../build/modules/vmec/vforces.mod"
  "../build/modules/vmec/VFORCES.mod"
  "CMakeFiles/vmec.dir/vforces.mod.stamp"

  "../build/modules/vmec/vmec_dim.mod"
  "../build/modules/vmec/VMEC_DIM.mod"
  "CMakeFiles/vmec.dir/vmec_dim.mod.stamp"

  "../build/modules/vmec/vmec_history.mod"
  "../build/modules/vmec/VMEC_HISTORY.mod"
  "CMakeFiles/vmec.dir/vmec_history.mod.stamp"

  "../build/modules/vmec/vmec_io.mod"
  "../build/modules/vmec/VMEC_IO.mod"
  "CMakeFiles/vmec.dir/vmec_io.mod.stamp"

  "../build/modules/vmec/vmec_main.mod"
  "../build/modules/vmec/VMEC_MAIN.mod"
  "CMakeFiles/vmec.dir/vmec_main.mod.stamp"

  "../build/modules/vmec/vmec_params.mod"
  "../build/modules/vmec/VMEC_PARAMS.mod"
  "CMakeFiles/vmec.dir/vmec_params.mod.stamp"

  "../build/modules/vmec/vmec_persistent.mod"
  "../build/modules/vmec/VMEC_PERSISTENT.mod"
  "CMakeFiles/vmec.dir/vmec_persistent.mod.stamp"

  "../build/modules/vmec/vmercier.mod"
  "../build/modules/vmec/VMERCIER.mod"
  "CMakeFiles/vmec.dir/vmercier.mod.stamp"

  "../build/modules/vmec/vspline.mod"
  "../build/modules/vmec/VSPLINE.mod"
  "CMakeFiles/vmec.dir/vspline.mod.stamp"

  "../build/modules/vmec/vsvd.mod"
  "../build/modules/vmec/VSVD.mod"
  "CMakeFiles/vmec.dir/vsvd.mod.stamp"

  "../build/modules/vmec/xstuff.mod"
  "../build/modules/vmec/XSTUFF.mod"
  "CMakeFiles/vmec.dir/xstuff.mod.stamp"
  )

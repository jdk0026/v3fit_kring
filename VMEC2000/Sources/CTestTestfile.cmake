# CMake generated Testfile for 
# Source directory: /home/inst/jdk0026/v3fit/trunk/VMEC2000/Sources
# Build directory: /home/inst/jdk0026/v3fit/trunk/VMEC2000/Sources
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(General)
subdirs(Hessian)
subdirs(Initialization_Cleanup)
subdirs(Input_Output)
subdirs(NESTOR_vacuum)
subdirs(Reconstruction)
subdirs(Splines)
subdirs(TimeStep)
